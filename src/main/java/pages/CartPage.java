package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CartPage {
    public CartPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = "//select[@id='shoppingcartForm:j_id_34_2_6:0:itemQuantity']")
    public WebElement DropDownQuantity;
    @FindBy(xpath = "//body//option[2]")
    public WebElement Quantity2;

    @FindBy(xpath = "//button[@id='shoppingcartForm:proceedToCheckout']")
    // @FindBy(xpath = "//button[@id='UploadFileDesktop']")
    // @FindBy(xpath = "//button[@id='itemConfiguratorForm:j_id_dt']")
  /*  @FindAll(
            {@FindBy(xpath = "//a[@id='UploadFileDesktop']"),
                    @FindBy(xpath = "//a[@id='itemConfiguratorForm:j_id_dt']"),
                    @FindBy(xpath = "//button[@id='shoppingcartForm:proceedToCheckout']")})

*/
    public WebElement ClickButton;

    @FindBy(xpath = "//*[contains(text(),'Bitte warte, bis die ganze Seite geladen wurde.')]")
    public WebElement Alert;

    @FindBy(xpath = "//*[@id='itemConfiguratorForm:quantity']")
    public WebElement Quantity;

    @FindBy(xpath = "//strong[contains(text(),'€')]")
    //     "//html/body/section[2]/div[1]/form/table/tbody/tr[8]/td[2]/strong") //TO DO Improve
    public WebElement TotalSum;
}

