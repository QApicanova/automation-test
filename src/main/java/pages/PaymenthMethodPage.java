package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class PaymenthMethodPage {
    public PaymenthMethodPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

  @FindBy(xpath = "//div[@id='creditCardRadio'] | " +
          "//div[@class='head js-accordion-title -expandable -active'] | " +
          "//input[@id='paymentMethod:j_id_2m_9:1:edyen'] ")
      //     "id='paymentMethod:j_id_2m_9:3:edyen'")
    public WebElement CreditCard;

    @FindBy(xpath = "//input[@id='cardHolder']")
    public WebElement CardName;
    @FindBy(xpath = "//input[@id='cardNumber']")
    public WebElement CardNumber;

    @FindBy(xpath = "//input[@id='cardExpirationMonth'] | //select[@id='cardExpirationMonth']")
    public WebElement Months;


    //            @FindBy(xpath = "//option[contains(text(),'10 - Oktober')]")
//            public WebElement Months10;
    @FindBy(xpath = "//input[@id='cardExpirationYear'] | //select[@id='cardExpirationYear']")
    public WebElement Years;
    /*  @FindBy(xpath = "//option[contains(text(),'2020')]")
      public WebElement Year2020;*/
    @FindBy(xpath = "//input[@id='cardSecurityCode']")
    public WebElement CvvCode;


  //  @FindBy(xpath = "//span[@class='ui-button-text ui-c']")
   @FindBy(xpath = "//button[@class='js-next-button action']")

  /*         "//button[@type='button'] | " +
           "//span[@class='ui-button-text ui-c'] | " +
           "//span[@class='button action -action -filled -arrow'] | " +

           "//div[@id='paymentMethod:proceed']//button[@type='button']//button[@class='js-next-button action'] | " +

           "//button[@class='js-next-button action'][contains(text(),'Continue')] | " +
           "//button[@type='button'][contains(text(),'Continue')] | " +
           "//button[@class='button action -action -filled -arrow']//span[@class='ui-button-text ui-c'][contains(text(),'Weiter')])")*/
    // @FindBy(xpath = "//button[@class='ui-button-text ui-c']")
    //  @FindBy(xpath = "//*[@id='paymentMethod:proceed']/button")
    // @FindBy(xpath = "//a[@type='button'] | //button[@type='button'] | //*[@id='creditCardData']/button")
    public WebElement clickButton;
}

