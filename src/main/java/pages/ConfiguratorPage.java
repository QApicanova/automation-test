package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ConfiguratorPage {
    public ConfiguratorPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }
    @FindBy(xpath = "//a[@id='UploadFileDesktop']")
    public WebElement ConfigButton;




    @FindBy(xpath = "//html/body/form[1]/section/div[1]/div[1]/div[2]/div[3]") //TO DO Improve
    public WebElement DesignButton;

    @FindBy(xpath = "//a[@id='itemConfiguratorForm:j_id_76']")
    public WebElement AddCart;

    @FindBy(xpath = "//*[@id='itemConfiguratorForm:quantity']")
    public WebElement Quantity;

    @FindBy(xpath = "//body//option[3]")
    public WebElement Quantity3;
    @FindBy(xpath = "//body//option[5]")
    public WebElement Quantity5;
    @FindBy(xpath = "//body//option[10]")
    public WebElement Quantity10;

    @FindBy(xpath = "//a[@href='/shoppingcart.jsf']")
    public WebElement Cart;
}



