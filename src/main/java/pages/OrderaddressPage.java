package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;

public class OrderaddressPage {
    public OrderaddressPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    //         @FindBy(xpath = "//table[@id='addressesForm:invoiceGender']")
//         public WebElement GenderDropDown;
    @FindBy(xpath = "//table[@id='addressesForm:invoiceGender']//label[contains(text(),'Herr')]")
    public WebElement Male;

    @FindBy(xpath = "//input[@id='addressesForm:invoiceForename'] | //input[@id='checkoutForm:invoiceForename']")
    public WebElement FirstName;

    @FindBy(xpath = "//input[@id='addressesForm:invoiceSurname'] | //input[@id='checkoutForm:invoiceSurname']")
    public WebElement LastName;


    @FindBy(xpath = "//input[@id='addressesForm:invoiceEmail'] | //input[@id='checkoutForm:invoiceEmail']")
    public WebElement EmailField;

 /*   @FindAll({
            @FindBy(xpath = "//input[@id='addressesForm:invoiceEmailConfirmation'] "),
            @FindBy(xpath = "//input[@id='addressesForm:invoiceEmail'] ")})
    public WebElement EmailFieldConfirm;
*/

    @FindBy(xpath = "//input[@id='addressesForm:invoiceStreet'] | //input[@id='checkoutForm:invoiceStreet']")
    public WebElement Address;

    @FindBy(xpath = "//input[@id='addressesForm:invoiceZipCode'] | //input[@id='checkoutForm:invoiceZipCode']")
    public WebElement PostalCode;

    @FindBy(xpath = "//input[@id='addressesForm:invoiceCity'] | //input[@id='checkoutForm:invoiceCity']")
    public WebElement City;

  //  @FindBy(xpath = "//div[@id='addressesForm:invoiceAddress']//button[@class='label'][contains(text(),'Deutschland')]")
    public WebElement CountryD;

    @FindAll({
            @FindBy(xpath = "//span[@data-dropdown='country']"),
            @FindBy(xpath = "//span[@data-dropdown='region']")})
    // @FindBy(xpath = "//div[@id='addressesForm:invoiceCountry']//button[@class='label'][contains(text(),'Danmark')]"
    //@FindBy(xpath = "//div[@id='addressesForm:invoiceRegion']//span[@class='form-dropdown']//input[@id='addressesForm:invoiceRegion'][contains(text(),'Alabama')]")
    public WebElement Region;


    @FindBy(xpath = "//ul[@class='choices']/li[2] | //ul[@class='choices country']/li[2]")
    public WebElement Region1;


    @FindBy(xpath = "//input[@id='addressesForm:invoicePhoneNumber'] | //input[@id='checkoutForm:invoicePhoneNumber']")
    public WebElement Telephone;

    @FindBy(xpath = "//button[@id='addressesForm:j_id_2u'] | " +
            "//button[@id='addressesForm:j_id_6u'] | " +
            "//button[@id='addressesForm:j_id_5x'] | " +
            "//button[@id='addressesForm:j_id_7e'] | " +
            "//button[@id='addressesForm:j_id_ai'] | " +
            "//button[@id='addressesForm:j_id_77'] | " +
        //    "//button[@id='addressesForm:j_id_7a'] | " +
            "//button[@type='submit'] | " +
            "//button[@id='addressesForm:j_id_5z'] ")

    // //div[@class='alternate locator 2'] | id('alternate locator 3')"
    //@FindBy(xpath = "//span[@class='ui-button-text ui-c'] | //button[@id='addressesForm:j_id_71'] | //button[@id='addressesForm:j_id_7e']")
    // @FindBy(xpath = "//button[@id='checkoutForm:saveInvoice']")
    //   @FindBy(xpath = "//*[@id='creditCardData']/button")
    // @FindBy(xpath = "//button[@id='checkoutForm:saveInvoice']")
    //@FindBy(xpath = "//button[@id='addressesForm:j_id_6v'] | //button[@id='checkoutForm:saveInvoice'] | //button[@id='creditCardData']")
    public WebElement ClickButtonWeiter;
}
