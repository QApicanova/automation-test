package pages.starmap;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class StarmapPage {
    public StarmapPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }
    //CTA button from LP
    @FindBy(xpath = "//button[@class='js-du-button action button -action -arrow filebutton shortbutton']")
    public WebElement uploadButtonStarmap;

    //CTA button from Config Starmap page
    @FindBy(xpath = "//a[@id='itemConfiguratorForm:j_id_5y']")

    public WebElement configuratorButton;
}


