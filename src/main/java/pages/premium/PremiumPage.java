package pages.premium;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class PremiumPage {
    public PremiumPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }
    @FindBy(xpath = "//a[@href='/foto-auf-heimtextilien/mundschutz-maske.jsf']")
    public WebElement ClickButton;

    @FindBy  (xpath = "//html/body/section[3]/div[1]/div[2]/div/div[2]/div[2]/div/a") //TO DO Improve
    public WebElement PremiumButton;
}



