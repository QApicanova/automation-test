package pages.canvas;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CanvasPage {

    public CanvasPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = "//div[contains(@class,'info')]//input[contains(@class,'js-du-file fileinput')]")
    //   @FindBy(xpath = "//div[contains(@class,'info')]//form[@class='js-du']//input[contains(@class,'js-du-file fileinput')] | //div[contains(@class,' upload-button')]//form[@type='file']//input[contains(@class,'js-du-file fileinput')] | //div[contains(@class,'col-xs-20 col-sm-8 price_table')]//input[contains(@class='pure-button pure-button-fake text-center')] | //div[@id='UploadFileTeaser'] | //div[@class='upload-button'] | //button[@class='js-du-button filebutton button action -arrow'] | //input[@id='uploadInputId533'] | //div[@data-product='233'] | //div[contains(@class, 'pure-button pure-button-fake text-center')]//div[contains(., 'Commandez ici')] | //div[@id='chooseFile']")

   /*        <div class="upload-block text-center">
            <button type="button" class="upload-button -camera -red" data-product="233">Fotos hochladen</button>
            <div class="multiUpload">
            <b>Jetzt mit Multi-Upload!</b>
            <small>Lade bis zu 10 Fotos gleichzeitig hoch.</small>
            </div>
            </div>

    })*/

    public WebElement UploadButton;
}
