package runner;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;


@RunWith(Cucumber.class)

@CucumberOptions(
        plugin = {"html:target/cucumber-report/smoketest", "json:target/cucumber.json", "pretty", "html:target/cucumber-reports"},
        features = "src/test/java/features",
        glue = "step/definition",
        tags = "@canvas")

public class TestRunner {

}
