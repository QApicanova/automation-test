Feature: Regular user navigates to Premium product, and payment is made with CreditCard

  @Premium
  Scenario Outline: User is available to buy Premium product

    Given User opens Chrome browser and move to the premium page of shop meinfoto
    When User provides valid credentials
    And User click on "click button" and move on the Premium page
    And  User click on "Premium button" and move to configurator page
#  And  Premium user is available to choose "design button"
    And  Premium user is available to choose "<quantity>"

    Then  User on cart page checks "<cost>" of masks

    And  User click on "CTA button" and move on orderaddress page
    And  User fill all "required fields" from delivery table
    And  User click on CTA button and move on  payment page
    And  User select "Credit Card" payment method
    And  User fill all "required fields" from credit card payment table
    And  User click on "CTA button" and move on confirmorder page
    And  User check that selected product is displayed correctly

    And  User click on "CTA button"
    Then User will be redirected on ordersuccessfully page and "check" tracking order

    Examples:
      |quantity|cost|
      |3|40,90 €|
      |5|85,00 €|
      |10|90,00 €|





