Feature: Towel product

  @test
  @towel
  @smoke
  Scenario Outline: User is available to choose product Towel

    Given User opens Chrome browser and move to the Towel page of "<shop>"
    When User click on "upload button" from Towel teaser
    Then User should move on configurator page with uploaded image
    And  User is available to choose needed "button"
 #   And  User is available to choose "quantity"
    And  User on cart page check that selected product is displayed correctly
    And  User click on "CTA button" and move on orderaddress page
    And  User fill all "required fields" from delivery table
    And  User click on CTA button and move on  payment page
    And  User select "Credit Card" payment method
    And  User fill all "required fields" from credit card payment table
    And  User click on "CTA button" and move on confirmorder page
    And  User check that selected product is displayed correctly
    And  User click on "CTA button"
    Then User will be redirected on ordersuccessfully page and "check" tracking order


    Examples:
      | shop                           |
  #  | meinfoto.de |

 #  | testing-photo-sur-toile-fr.picanova.de  |
  #    | testing-meinxxl-de.picanova.de          |
  #- button on config page

 #   | testing-canvasdiscount-com.picanova.com |
  #- region
   #   | testing-bestcanvas-ca.picanova.com      |
  #- region
  #    | testing-canvasonsale-com.picanova.com |
  #- region


  #   | testing-bestecanvas-nl.picanova.de      | - CC select

      | testing-mi-arte-es.picanova.de |
#D      | testing-stampa-su-tela-it.picanova.de |
#       | testing-my-picture-co-uk.picanova.de  |
# D     | testing-my-picture-co-uk.picanova.de/photo-prints/canvas-prints.jsf |


        #  code post
 #     | testing-monoeuvre-fr.picanova.de |


#      # email confirmation
#      | testing-bestcanvas-se.picanova.de     |
#
