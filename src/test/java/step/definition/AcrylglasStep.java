package step.definition;

import base.BaseUtil;
import pages.acrylglas.AcrylglasPage;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import pages.canvas.CanvasPage;

import java.util.concurrent.TimeUnit;


public class AcrylglasStep extends BaseUtil {

        private BaseUtil base;

        public AcrylglasStep(BaseUtil base) {
            this.base = base;
        }
        @Given("^User opens Chrome browser and move to the acrylglas page of \"([^\"]*)\"$")
        public void userOpensChromeBrowserAndMoveToTheAcrylglasPageOfShopMeinfoto(String address) {
            base.Driver.navigate().to("https://test:karneval2@" + address);
            base.Driver.manage().window().maximize();

        }

        @When("^User click on \"([^\"]*)\" from Acryglas teaser$")
        public void userClickOnFromAcryglasTeaser(String arg0) throws InterruptedException {
         base.Driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
         AcrylglasPage page = new AcrylglasPage(base.Driver);
         page.uploadButtonAcrylglas.sendKeys(System.getProperty("user.dir") + "/src/main/resources/k.jpg");

        }





}
