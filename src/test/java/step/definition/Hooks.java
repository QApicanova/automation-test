package step.definition;

import base.BaseUtil;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import org.junit.Assert;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;


public class Hooks extends BaseUtil {

    private BaseUtil base;
    // public static final File PhantomJS_EXE = new File(System.getProperty("user.dir"), "/Users/yaz/pica/src/main/resources/phantomjs-2.1.1-macosx");
    //"src/tools/phantomjs-2.1.1-windows/bin/phantomjs.exe");

    public Hooks(BaseUtil base) {
        this.base = base;
    }

    @Before
    public void InitializeTest() {
        String os = System.getProperty("os.name").toLowerCase();
        if(os.contains("mac")) {
            System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver");
        }else {
            System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
        }
        base.Driver = new ChromeDriver();
      /*  base.Driver = new PhantomJSDriver();

        DesiredCapabilities caps = new DesiredCapabilities();
        caps.setJavascriptEnabled(true);
        caps.setCapability("phantomjs.binary.path", PhantomJS_EXE.getAbsolutePath()); */

        ExpectedCondition<Boolean> expectation = new
                ExpectedCondition<Boolean>() {
                    public Boolean apply(WebDriver driver) {
                        return ((JavascriptExecutor) driver).executeScript("return document.readyState").toString().equals("complete");


                    }
                };
        try {
            Thread.sleep(1000);
            WebDriverWait wait = new WebDriverWait(base.Driver, 30);
            wait.until(expectation);
        } catch (Throwable error) {
            Assert.fail("Timeout waiting for Page Load Request to complete.");
        }
    }


    @After
    public void TearDownTest(Scenario scenario) {
        if (scenario.isFailed()) {
            System.out.println(scenario.getName());
        }

    //    base.Driver.quit();
    }
}
