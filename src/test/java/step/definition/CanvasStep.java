package step.definition;

import base.BaseUtil;

import org.openqa.selenium.Keys;
import org.openqa.selenium.UnhandledAlertException;

import pages.canvas.CanvasPage;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.interactions.Actions;

import pages.*;

import java.util.concurrent.TimeUnit;


public class CanvasStep extends BaseUtil {


    private BaseUtil base;


    public CanvasStep(BaseUtil base) {
        this.base = base;

    }

    @Given("^User opens Chrome browser and move to the canvas page of \"([^\"]*)\"$")
    public void userOpensChromeBrowserAndMoveToTheCanvasPageOfShop(String address) {
//        base.Driver.navigate().to("https://www.meinfoto.de/foto-in-gross/foto-auf-leinwand.jsf");
//        base.Driver.manage().window().maximize();
        base.Driver.navigate().to("https://test:karneval2@" + address);
        base.Driver.manage().window().maximize();

    }

    @When("^User click on \"([^\"]*)\" from teaser$")
    public void userClickOnFromTeaser(String button) {
        base.Driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        CanvasPage page = new CanvasPage(base.Driver);
       // page.UploadButton.click();
        page.UploadButton.sendKeys(System.getProperty("user.dir") + "/src/main/resources/k.jpg");

    }

    @Then("^User should move on configurator page with uploaded image$")
    public void userShouldMoveOnConfiguratorPageWithUploadedImage() {
        base.Driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
    }

    @And("^User is available to choose needed \"([^\"]*)\"$")
    public void userIsAvailableToChooseNeededFormat(String button) {
        base.Driver.manage().timeouts().implicitlyWait(7, TimeUnit.SECONDS);
        ConfiguratorPage page = new ConfiguratorPage(base.Driver);

        Actions actions = new Actions(base.Driver);
        actions.moveToElement(page.ConfigButton);
        actions.perform();

        page.ConfigButton.click();
    }

    @And("^User on cart page check that selected product is displayed correctly$")
    public void userOnCartPageCheckThatSelectedProductIsDisplayedCorrectly() {
    }

  /*  @And("^User is available to choose \"([^\"]*)\"$")
    public void userIsAvailableToChoose(String arg0) throws InterruptedException {
       // base.Driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        CartPage page = new CartPage(base.Driver);
        Actions actions = new Actions(base.Driver);
        actions.moveToElement(page.DropDownQuantity);
        actions.perform();
        Thread.sleep(3000);
        page.DropDownQuantity.click();
        Thread.sleep(3000);
        page.Quantity2.click();
        base.Driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);

    }*/

    @And("^User click on \"([^\"]*)\" and move on orderaddress page$")
    public void userClickOnAndMoveOnOrderaddressPage(String button) throws InterruptedException {
        CartPage page = new CartPage(base.Driver);
        //   UnhandledAlertException = new UnhandledAlertException(String text);
        Thread.sleep(3000);
        page.ClickButton.click();
/*
        if @alert.button(:text, "Bitte warte, bis die ganze Seite geladen wurde.").present?
                button.click();
	else
        fail("Button with text '#{text}' is not visible.");
        end
                end;*/

        //if  unexpected alert open: {Alert text : Bitte warte, bis die ganze Seite geladen wurde. Wenn die Vorschau nicht angezeigt wird, lade die Seite neu.}
    }

    @And("^User fill all \"([^\"]*)\" from delivery table$")
    public void userFillAllFromDeliveryTable(String arg0) throws InterruptedException {
        OrderaddressPage page = new OrderaddressPage(base.Driver);

        base.Driver.manage().window().maximize();
        Thread.sleep(2000);


        //      page.GenderDropDown.click();
        //      page.Male.click();
        page.FirstName.sendKeys("AUTOMATION");
        page.LastName.sendKeys("TEST");
        page.EmailField.sendKeys("picanovatest@gmail.com");

  /*      page.EmailFieldConfirm.clear();
        page.EmailFieldConfirm.sendKeys("picanovatest@gmail.com");
*/
        //  page.EmailFieldConfirm.sendKeys(Keys.chord(Keys.CONTROL, "a"), "picanovatest@gmail.com");

        page.Address.sendKeys("#reject10");
        page.PostalCode.sendKeys("28009");
        page.City.sendKeys("TEST");

      //  page.Country.click();


        page.Region.click();
        page.Region1.click();


        page.Telephone.sendKeys("+7374578123");


        Actions actions = new Actions(base.Driver);
        actions.moveToElement(page.ClickButtonWeiter);
        actions.perform();
        page.ClickButtonWeiter.click();
    }

 /*   @And("^User click on \"([^\"]*)\" and move on payment page$")
    public void userClickOnCTAButtonAndMoveOnPaymentPage() {
    }*/


    @And("^User select \"([^\"]*)\" payment method$")
    public void userSelectPaymentMethod(String arg0) throws InterruptedException {
        PaymenthMethodPage page = new PaymenthMethodPage(base.Driver);
        base.Driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        // Thread.sleep(2000);

        Actions actions = new Actions(base.Driver);
        actions.moveToElement(page.CreditCard);
        actions.perform();
        page.CreditCard.click();
    }

    @And("^User fill all \"([^\"]*)\" from credit card payment table$")
    public void userFillAllFromCreditCardPaymentTable(String arg0) {
        PaymenthMethodPage page = new PaymenthMethodPage(base.Driver);
        page.CardName.sendKeys("Bijenkorf");
        page.CardNumber.sendKeys("5100060000000002");
        page.Months.sendKeys("12");

        page.Years.sendKeys("29");
        //  page.Year2020.click();
        page.CvvCode.sendKeys("737");
    }

    @And("^User click on \"([^\"]*)\" and move on confirmorder page$")
    public void userClickOnAndMoveOnConfirmorderPage(String arg0) {
        PaymenthMethodPage page = new PaymenthMethodPage(base.Driver);

        base.Driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

/* Actions actions = new Actions(base.Driver);
        actions.moveToElement(page.clickButtonWeiter);
        actions.perform();*/

        page.clickButton.click();
        // base.Driver.close();
    }

    @And("^User check that selected product is displayed correctly$")
    public void userCheckThatSelectedProductIsDisplayedCorrectly() {
    }

    @And("^User click on \"([^\"]*)\"$")
    public void userClickOn(String button) {
        ConfirmOrderPage page = new ConfirmOrderPage(base.Driver);
        page.ConfirmPurchaseButton.click();
    }

    @Then("^User will be redirected on ordersuccessfully page and \"([^\"]*)\" tracking order$")
    public void userWillBeRedirectedOnOrdersuccessfullyPageAndTrackingOrder(String arg0) {
        base.Driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        base.Driver.close();
    }

}
