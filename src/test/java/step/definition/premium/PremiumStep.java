package step.definition.premium;

import base.BaseUtil;
import pages.CartPage;
import pages.ConfiguratorPage;
import pages.premium.PremiumPage;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.interactions.Actions;

import java.util.concurrent.TimeUnit;

import static org.testng.AssertJUnit.assertEquals;


public class PremiumStep extends BaseUtil {


    private BaseUtil base;


    public PremiumStep(BaseUtil base) throws InterruptedException {
        this.base = base;

    }

    @Given("^User opens Chrome browser and move to the premium page of shop meinfoto$")
    public void userOpensMainPage() {
        base.Driver.navigate().to("https://test:karneval2@testing.meinfoto.de");
        base.Driver.manage().window().maximize();

    }

    @When("^User provides valid credentials$")
    public void userProvidesCredentials() {
        base.Driver.navigate().to("https://testing.meinfoto.de");
        base.Driver.manage().window().maximize();
    }

    @And("^User click on \"([^\"]*)\" and move on the Premium page$")
    public void userClicksOnZurCustomask(String ClickButton) {
        base.Driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        PremiumPage page = new PremiumPage(base.Driver);
        page.ClickButton.click();
    }

    @And("^User click on \"([^\"]*)\" and move to configurator page$")
    public void userClickOnPremiumButtonAndMoveToConfigPage(String PremiumButton) {
        base.Driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        PremiumPage page = new PremiumPage(base.Driver);
        page.PremiumButton.click();
    }

  /*  @And("^Premium user is available to choose \"([^\"]*)\"$")
    public void userIsAvailableToChooseDesign(String DesignButton) {
        base.Driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        ConfiguratorPage page = new ConfiguratorPage(base.Driver);
        page.DesignButton.click();
    }*/

    @And("^Premium user is available to choose \"([^\"]*)\"$")
    public void userPremiumIsAvailableToChooseQuantity(String quantity) throws InterruptedException {

        base.Driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        ConfiguratorPage page = new ConfiguratorPage(base.Driver);
        Actions actions = new Actions(base.Driver);
        actions.moveToElement(page.Quantity);
        actions.perform();
        Thread.sleep(3000);
        page.Quantity.click();
        //   Thread.sleep(3000);
        base.Driver.findElement(By.xpath("//body//option[" + quantity + "]")).click();


        actions.moveToElement(page.AddCart);
        page.AddCart.click();
        Thread.sleep(3000);
        page.Cart.click();
        base.Driver.manage().timeouts().implicitlyWait(4, TimeUnit.SECONDS);
    }

    @Then("^User on cart page checks \"([^\"]*)\" of masks$")

    public void the_result_should_be(String expectedTotalSum) {
        CartPage page = new CartPage(base.Driver);
        String TotalSum = page.TotalSum.getText();


        assertEquals(expectedTotalSum, TotalSum);


    }

    @And("^User click on \"([^\"]*)\" and move on OrderAddress page$")
    public void userClickOnAndMoveOnOrderAddressPage(String arg0) {
        CartPage page = new CartPage(base.Driver);
        page.ClickButton.click();
    }
}

