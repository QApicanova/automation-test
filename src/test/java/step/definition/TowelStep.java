package step.definition;

import base.BaseUtil;
import pages.canvas.CanvasPage;
import pages.towel.TowelPage;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;

import java.util.concurrent.TimeUnit;

public class TowelStep extends BaseUtil {

    private BaseUtil base;

    public TowelStep(BaseUtil base) {
        this.base = base;

    }

    @Given("^User opens Chrome browser and move to the Towel page of \"([^\"]*)\"$")
    public void userOpensChromeBrowserAndMoveToTheCanvasPageOfShop(String address) {
//        base.Driver.navigate().to("https://www.meinfoto.de/foto-in-gross/foto-auf-leinwand.jsf");
//        base.Driver.manage().window().maximize();
        base.Driver.navigate().to("https://test:karneval2@" + address);
        base.Driver.manage().window().maximize();
    }

    @When("^User click on \"([^\"]*)\" from Towel teaser$")
    public void userClickOnFromTeaser(String button) {
        base.Driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        TowelPage page = new TowelPage(base.Driver);
        // page.UploadButton.click();
        page.UploadButton.sendKeys(System.getProperty("user.dir") + "/src/main/resources/k.jpg");

    }
}